const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const competenceRouter = require('./src/app/routers/Competence');
const directionRouter = require('./src/app/routers/Direction');
const domaineRouter = require('./src/app/routers/Domaine');
const emploiRouter = require('./src/app/routers/Emploi');
const niveauRouter = require('./src/app/routers/Niveau');
const sousDomaineRouter = require('./src/app/routers/SousDomaine');
const emploiNiveauxRouter = require('./src/app/routers/EmploiNiveaux');

const app = express();
const port = 3000;

app.listen(port,
    () => console.log(`Simple Express pp listening on port ${port}!`));

    // support parsing of application/json type post data
app.use(bodyParser.json());

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
//Use competence router
app.use('/competences',competenceRouter);

//Use direction router
app.use('/directions',directionRouter);

//Use domaine router
app.use('/domaines',domaineRouter);

//Use emploi router
app.use('/emplois',emploiRouter);

//Use niveau router
app.use('/niveaux',niveauRouter);

//Use sous domaine router
app.use('/sousdomaines',sousDomaineRouter);

//Use sous emploi niveaux router
app.use('/emploiniveaux',emploiNiveauxRouter);


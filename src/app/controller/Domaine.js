const domaine = require('../modules/Domaine').Domaine;

function getAllDomaines(req, res){
    domaine.findAll()
    .then(data => res.send(data))
    .catch(error => console.log(error));
}

function getDomaineById(req, res){
    var _id = req.body.id;
    domaine.findAll({
        where : {
            id: _id
        }
    })
    .then(data => res.send(data))
    .catch(error => console.log(error))
}

module.exports = {
    getAllDomaines,
    getDomaineById
};
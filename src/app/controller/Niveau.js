const niveau = require('../modules/Niveau').Niveau;
const competence = require('../modules/Competence').Competence;

function getAllNiveaux(req, res){
    niveau.findAll({
        include: [
            {model: competence, as: "competence" },
        ]
    })
    .then(data => res.send(data))
    .catch(error => console.log(error));
}

function getNiveauById(req, res){
    var _id = req.body.id;
    niveau.findAll({
        where : {
            id: _id
        }
    })
    .then(data => res.send(data))
    .catch(error => console.log(error))
}

module.exports = {
    getAllNiveaux,
    getNiveauById
};
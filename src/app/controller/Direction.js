const direction = require('../modules/Direction').Direction;

function getAllDirections(req, res){
    direction.findAll()
    .then(data => res.send(data))
    .catch(error => console.log(error));
}

function getDirectionById(req, res){
    var _id = req.body.id;
    direction.findAll({
        where : {
            id: _id
        }
    })
    .then(data => res.send(data))
    .catch(error => console.log(error))
}

module.exports = {
    getAllDirections,
    getDirectionById
};
const Sequelize = require('sequelize');
const emploi = require('../modules/Emploi').Emploi;
const Niveau = require('../modules/Niveau').Niveau;
const Competence = require('../modules/Competence').Competence;
const SousDomaine = require('../modules/SousDomaine').SousDomaine;
const Direction = require('../modules/Direction').Direction;


function getAllEmplois(req, res){
    emploi.findAll({
        include: [{
            model: Niveau, as: 'levels',
            include : [
                {
                    model: Competence,
                    as: 'competence',
                    include: [
                        {
                            model: SousDomaine,
                            as: 'sous_domaine'
                        }
                    ]
                }
            ]
            // through: {
            //   attributes: ['id_emploi', 'id_niveau'],
            // }
            },
            {
                model: Direction,
                as: 'direction'
            }
        ]
    })
    .then(data => res.send({count: data.length, data}))
    .catch(error => console.log(error));
}

function getJobsBySkills(req, res) {
    console.log(req.query)
    const skillArray = JSON.parse(req.query.skills);
    emploi.findAll({
        
        include: [{
            model: Niveau, as: 'levels',
            where: {
                id_competence : {
                    [Sequelize.Op.in]: skillArray
                }
            },
            include : [
                {
                    model: Competence,
                    as: 'competence',
                    include: [
                        {
                            model: SousDomaine,
                            as: 'sous_domaine'
                        }
                    ]
                }
            ]
            // through: {
            //   attributes: ['id_emploi', 'id_niveau'],
            // }
            },
            {
                model: Direction,
                as: 'direction'
            }
        ]
    })
    .then(data => res.send({count: data.length, data}))
    .catch(error => console.log(error));
}

function getEmploiById(req, res){
    var _id = req.body.id;
    emploi.findAll({
        where : {
            id: _id
        }
    })
    .then(data => res.send(data))
    .catch(error => console.log(error))
}

function fetchMe(req, res) {
    const nom = req.decoded.data;
    return models.Emploi
    .find({
      include: [{
        model: models.Niveau,
        as: 'niveaux',
        required: false,
        attributes: ['id_niveau', 'nom'],
        through: { attributes: [] }
      }],
      where: { nom }
    })
} 
module.exports = {
    getAllEmplois,
    getEmploiById,
    fetchMe,
    getJobsBySkills
};
const sousDomaine = require('../modules/SousDomaine').SousDomaine;

function getAllSousDomaines(req, res){
    sousDomaine.findAll()
    .then(data => res.send(data))
    .catch(error => console.log(error));
}

function getSousDomaineById(req, res){
    var _id = req.body.id;
    sousDomaine.findAll({
        where : {
            id: _id
        }
    })
    .then(data => res.send(data))
    .catch(error => console.log(error))
}

module.exports = {
    getAllSousDomaines,
    getSousDomaineById
};
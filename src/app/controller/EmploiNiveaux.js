const emploiNiveau = require('../modules/EmploiNiveaux').EmploiNiveau;
const niveau = require('../modules/Niveau').Niveau;
const emploi = require('../modules/Emploi').Emploi;


function getAllEmploiNiveaux(req, res){
    emploiNiveau.findAll({
        include: [
            {model: niveau, as: "niveau_competence" },
            {model: emploi, as: "emploi" }
        ],
       
    })
    .then(data => res.send(data))
    .catch(error => console.log(error));
    
}

function getEmploiNiveauxById(req, res){
    var _id = req.body.id;
    emploi.findAll({
        where : {
            id: _id
        }
    })
    .then(data => res.send(data))
    .catch(error => console.log(error))
}

module.exports = {
    getAllEmploiNiveaux,
    getEmploiNiveauxById
};

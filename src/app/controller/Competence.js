const competence = require('../modules/Competence').Competence;
const SousDomaine = require('../modules/SousDomaine').SousDomaine;

function getAllCompetences(req, res){
    competence.findAll({
        include: [
            {model: SousDomaine, as: "sous_domaine" }
        ]
    })
    .then(data => res.send(data))
    .catch(error => console.log(error));
}

function getCompetenceById(req, res){
    var _id = req.body.id;
    competence.findAll({
        where : {
            id: _id
        }
    })
    .then(data => res.send(data))
    .catch(error => console.log(error))
}

function addSkill(req, res) {
    const { name , description, sousdomaine_id } = req.body;
    competence.create({ nom: name, description, id_sousdomaine: sousdomaine_id })
        .then(result => {
            res.send(result)
        })
        .catch(error => res.send(error));
}

module.exports = {
    getAllCompetences,
    getCompetenceById,
    addSkill
};
const express = require('express');
const router = express.Router();

const competenceController = require('../controller/Competence');



router.get('/getAll', competenceController.getAllCompetences);


router.get('/getByID', competenceController.getCompetenceById);

router.post('/add', competenceController.addSkill);



module.exports = router;
const express = require('express');
const router = express.Router();

const emploiController = require('../controller/Emploi');



router.get('/getAll', emploiController.getAllEmplois);


router.get('/getByID', emploiController.getEmploiById);

router.post('/getBySkills', emploiController.getJobsBySkills);



module.exports = router;
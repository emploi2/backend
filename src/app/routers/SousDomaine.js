const express = require('express');
const router = express.Router();

const sousDomaineController = require('../controller/SousDomaine');



router.get('/getAll', sousDomaineController.getAllSousDomaines);


router.get('/getByID', sousDomaineController.getSousDomaineById);


module.exports = router;
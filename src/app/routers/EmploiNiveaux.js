const express = require('express');
const router = express.Router();

const emploiNiveauxController = require('../controller/EmploiNiveaux');



router.get('/getAll', emploiNiveauxController.getAllEmploiNiveaux);


router.get('/getByID', emploiNiveauxController.getEmploiNiveauxById);


module.exports = router;
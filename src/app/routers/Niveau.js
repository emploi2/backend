const express = require('express');
const router = express.Router();

const niveauController = require('../controller/Niveau');



router.get('/getAll', niveauController.getAllNiveaux);


router.get('/getByID', niveauController.getNiveauById);


module.exports = router;
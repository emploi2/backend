const express = require('express');
const router = express.Router();

const directionController = require('../controller/Direction');



router.get('/getAll', directionController.getAllDirections);


router.get('/getByID', directionController.getDirectionById);


module.exports = router;
const express = require('express');
const router = express.Router();

const domaineController = require('../controller/Domaine');



router.get('/getAll', domaineController.getAllDomaines);


router.get('/getByID', domaineController.getDomaineById);


module.exports = router;
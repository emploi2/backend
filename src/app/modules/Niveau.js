const {Sequelize, Model} = require('sequelize');
const mySequelize = require('../database/SqlConnect');

const Competence = require('./Competence').Competence;



class Niveau extends Model{}

Niveau.init({
    id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
    nom: Sequelize.STRING,
    numero_niveau: Sequelize.INTEGER,
    description: Sequelize.STRING,
    id_competence : Sequelize.INTEGER,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
}, {sequelize: mySequelize, modelName: 'niveau', freezeTableName: true});

//Niveau.hasOne(competence, {as: 'competence', foreignKey:'id_competence'});
Niveau.belongsTo(Competence, {as: 'competence', foreignKey:'id_competence'});

module.exports.Niveau = Niveau;
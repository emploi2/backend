const {Sequelize, Model} = require('sequelize');
const mySequelize = require('../database/SqlConnect');

const Emploi = require('./Emploi').Emploi;
const Niveau = require('./Niveau').Niveau;



class EmploiNiveau extends Model{}


EmploiNiveau.init({

    id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
    id_emploi: Sequelize.INTEGER,
    id_niveau: Sequelize.INTEGER,
    justification: Sequelize.STRING,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,

}, {sequelize: mySequelize, modelName: 'emploi_niveaux', freezeTableName: true});



Niveau.belongsToMany(Emploi, {as: 'jobs',through: EmploiNiveau, foreignKey: 'id_niveau'});
Emploi.belongsToMany(Niveau, {as: 'levels',through: EmploiNiveau, foreignKey: 'id_emploi'});





module.exports = EmploiNiveau;
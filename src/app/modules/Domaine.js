const {Sequelize, Model} = require('sequelize');
const mySequelize = require('../database/SqlConnect');

class Domaine extends Model{}


Domaine.init({
    id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
    nom: Sequelize.STRING,
    description: Sequelize.STRING,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
}, {sequelize: mySequelize, modelName: 'domaine', freezeTableName: true});




module.exports.Domaine = Domaine;
const {Sequelize, Model} = require('sequelize');
const mySequelize = require('../database/SqlConnect');

const direction = require('./Direction').Direction;


class Emploi extends Model{}


Emploi.init({
    id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
    nom: Sequelize.STRING,
    description: Sequelize.STRING,
    id_direction: Sequelize.INTEGER,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
}, {sequelize: mySequelize, modelName: 'emploi', freezeTableName: true});


Emploi.belongsTo(direction, {as: 'direction', foreignKey:'id_direction'});


module.exports.Emploi = Emploi;


const {Sequelize, Model} = require('sequelize');
const mySequelize = require('../database/SqlConnect');

const sousDomaine = require('./SousDomaine').SousDomaine;
//const niveau = require('./Niveau').Niveau;

class Competence extends Model{}

Competence.init({
    id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
    nom: Sequelize.STRING,
    description: Sequelize.STRING,
    actions_liees: Sequelize.STRING,
    id_sousdomaine: Sequelize.INTEGER,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
}, {sequelize: mySequelize, modelName: 'competence', freezeTableName: true});

Competence.belongsTo(sousDomaine, {as: 'sous_domaine', foreignKey:'id_sousdomaine'});

module.exports.Competence = Competence;
const {Sequelize, Model} = require('sequelize');
const domaine = require('./Domaine').Domaine;
const mySequelize = require('../database/SqlConnect');


class SousDomaine extends Model{}

SousDomaine.init({
    id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
    nom: Sequelize.STRING,
    description: Sequelize.STRING,
    id_domaine: Sequelize.INTEGER,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
}, {sequelize: mySequelize, modelName: 'sousDomaine', freezeTableName: true});

SousDomaine.belongsTo(domaine, {as: 'domaine', foreignKey:'id_domaine'});

module.exports.SousDomaine = SousDomaine;
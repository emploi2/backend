const {Sequelize, Model} = require('sequelize');
const mySequelize = require('../database/SqlConnect');

class Direction extends Model{}

Direction.init({
    id: {type: Sequelize.INTEGER, allowNull: false, autoIncrement: true, primaryKey: true},
    nom: Sequelize.STRING,
    description: Sequelize.STRING,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
}, {sequelize: mySequelize, modelName: 'direction', freezeTableName: true});




module.exports.Direction = Direction;
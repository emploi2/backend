const Sequelize = require('sequelize');

const mySequelize = new Sequelize('emploi', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

mySequelize.authenticate()
    .then(() => console.log('Authentication success'))
    .catch(error => console.log(error));



module.exports = mySequelize;
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2020 at 11:55 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emploi`
--

-- --------------------------------------------------------

--
-- Table structure for table `competence`
--

CREATE TABLE `competence` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `actions_liees` text NOT NULL,
  `id_sousdomaine` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `competence`
--

INSERT INTO `competence` (`id`, `nom`, `description`, `actions_liees`, `id_sousdomaine`, `createdAt`, `updatedAt`) VALUES
(1, 'Transmettre des informations à ses collègues', 'Capacité à fournir une information claire et complète permettant au destinataire d\'agir en autonomie.', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Conduire l\'entretien du patrimoine.', 'Capacité à détecter des anomalies et mettre en œuvre une réponse adaptée.', '', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Piloter des indicateurs de performance', 'Capacité à établir des indicateurs, à en assurer le suivi et à en analyser les résultats.', '', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Améliorer le cadre de vie résidentiel', 'Capacité à garantir et développer un cadre de vie paisible et agréable aux locataires.', '', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Superviser l\'action d\'une équipe', 'Capacité à gérer l\'activité d\'une équipe sur le plan administratif et humain.', '', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Contrôler l\'évolution de l\'impayé', 'Capacité à infléchir l\'évolution de l\'impayé en mettant en place des actions individualisées', '', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Conseiller un locataire', 'Capacité à apporter une réponse construite et compréhensible à un locataire en justifiant la position de l\'entreprise.', '', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Accompagner un public en difficulté', 'Capacité à détecter les familles en difficulté et les accompagner vers la société', '', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Organiser son environnement de travail ', 'Capacité à structurer son travail pour faire face aux imprévus tout en tenant compte de l\'entreprise et de ses collègues.', '', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Promouvoir l\'image de l\'entreprise', 'Capacité à représenter l\'entreprise et à en véhiculer une image positive auprès des partenaires extérieurs.', '', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Accroître les compétences de son équipe', 'Capacité à accompagner ses collaborateurs dans leur montée en compétences tant individuelles que collective', '', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Suivre un budget', 'Capacité à constituer et suivre la réalisation d\'un budget', '', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Suivre un dossier administratif', 'Capacité à prendre en charge un dossier', '', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `direction`
--

CREATE TABLE `direction` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `direction`
--

INSERT INTO `direction` (`id`, `nom`, `description`, `createdAt`, `updatedAt`) VALUES
(1, 'DAT', 'Direction de action territoriale', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'DPSQA', 'Direction ', '2020-04-01 07:08:09', '2020-04-01 07:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `domaine`
--

CREATE TABLE `domaine` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `domaine`
--

INSERT INTO `domaine` (`id`, `nom`, `description`, `createdAt`, `updatedAt`) VALUES
(1, 'Savoir', 'Ensemble des connaissances', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Savoir-Faire', 'Ensembles des compétences ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Savoir-être', 'Manière d\'être', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `emploi`
--

CREATE TABLE `emploi` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `id_direction` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_niveau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emploi`
--

INSERT INTO `emploi` (`id`, `nom`, `description`, `id_direction`, `createdAt`, `updatedAt`, `id_niveau`) VALUES
(1, 'Gardien', 'Assurer l\'intégrité du patrimoine', 1, '2020-04-01 07:05:39', '2020-04-01 07:05:39', 0),
(2, 'Responsable de site', 'Manager les gardiens', 1, '2020-04-01 07:05:39', '2020-04-01 07:05:39', 0),
(3, 'Conseiller gestion locataires', 'Gérer les locataires', 1, '2020-04-01 07:06:47', '2020-04-01 07:06:47', 0),
(4, 'Adjoint au chef d\'agence', 'Seconder le chef d\'agence', 1, '2020-04-01 07:06:47', '2020-04-01 07:06:47', 0),
(5, 'Agent d\'accueil', 'Accueillir le public', 1, '2020-04-01 07:07:25', '2020-04-01 07:07:25', 0),
(6, 'Conseillère sociale', 'Faire le pont entre l\'entreprise et les partenaires', 2, '2020-04-01 07:09:04', '2020-04-01 07:09:04', 0),
(7, 'Agent contentieux', 'S\'occuper du contentieux', 2, '2020-04-01 07:09:46', '2020-04-01 07:09:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `emploi_niveaux`
--

CREATE TABLE `emploi_niveaux` (
  `id` int(11) NOT NULL,
  `id_emploi` int(11) NOT NULL,
  `id_niveau` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emploi_niveaux`
--

INSERT INTO `emploi_niveaux` (`id`, `id_emploi`, `id_niveau`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 2, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 2, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 2, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 2, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 2, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 2, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 2, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 2, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 2, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 3, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 3, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 3, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 3, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 3, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 3, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 3, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 4, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 4, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 4, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 4, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 4, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 4, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 4, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 4, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 4, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 5, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 5, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 6, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 6, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 6, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 6, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 6, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 6, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 6, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 7, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 7, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 7, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 7, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 7, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `niveau`
--

CREATE TABLE `niveau` (
  `id` int(11) NOT NULL,
  `numero_niveau` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `id_competence` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `niveau`
--

INSERT INTO `niveau` (`id`, `numero_niveau`, `nom`, `description`, `id_competence`, `createdAt`, `updatedAt`) VALUES
(1, 3, 'Transmettre une information simple, pertinente et compréhensible.', 'En suivant la procédure en vigueur. En identifiant l\'interlocuteur concerné. En utilisant le bon canal de communication', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 'Transmettre une information complexe de manière structurée et compréhensible.', 'En consolidant les informations. En synthétisant les avis divergents. En s\'adaptant (discours, support) à ses interlocuteurs.', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 'Conduire l\'entretien courant du patrimoine. En contrôlant les prestations réalisées par les entrepri', 'En contrôlant les prestations réalisées. En détectant les anomalies. En mettant en place des solutions pour résoudre les anomalies.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 'Conduire l\'entretien exceptionnel du patrimoine. ', 'En diagnostiquant les dysfonctionnements. En proposant des solutions adaptées. En chiffrant les réparations.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 'Piloter des indicateurs individuels', 'En mettant en œuvre des actions adaptées. En réajustant ses actions pour améliorer sa performance. ', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 'Piloter des indicateurs collectifs', 'En assurant des feedbacks réguliers. En réajustant les indicateurs.', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 'Préserver le cadre de vie résidentiel', 'En facilitant l\'intervention des pouvoirs publics. En contribuant à la résolution des litiges entre voisins. En veillant au respect du règlement intérieur. En identifiant les fauteurs de trouble.', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, 'Développer le bien-vivre ensemble.', 'En aidant les actions de bien-vivre ensemble. En intégrant les locataires dans les démarches. En relayant les actions impulsées par l\'entreprise. En impulsant des actions.', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 1, 'Superviser une équipe mono disciplinaire', 'En participant à la gestion des ressources humaines. En assurant la performance et la continuité de l\'activité.', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 'Superviser une équipe pluridisciplinaire', 'En s\'assurant de la synergie entre les différents emplois. ', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 1, 'Gérer un secteur', 'En s\'assurant du bon déroulé des différentes procédures.', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 2, 'Avoir une vision plus globale', 'En observant envisageant l\'ensemble de l\'entreprise', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 1, 'Apporter une réponse générale de premier niveau.', 'En renseignant selon la documentation usuelle de l\'entreprise.', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 2, 'Apporter une réponse spécifique de 2nd niveau', 'En comprenant le problème soulevé. En recherchant l\'information. En s\'assurant de la bonne compréhension du locataire. En faisant émerger une compréhension commune du problème.', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 1, 'Détecter et signaler une famille en difficulté', 'En orientant vers la personne compétente. En alertant sur un situation à risque grâce à un faisceau d\'indices.', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 2, 'Mettre en œuvre des solutions pour accompagner une famille en difficulté', 'En se coordonnant avec des partenaires extérieurs. En accompagnant la famille.', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, 'Organiser son environnement de travail pour être performant', 'En planifiant les tâches à réaliser. En hiérarchisant constamment les tâches à réaliser.', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 2, 'Organiser son environnement de travail en tenant compte de ses collègues et de la continuité du serv', 'En ayant une vision claire du travail de ses collègues. En tenant compte des impératifs et besoins de ses collègues. En anticipant ses absences.', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 1, 'Entretenir des relations professionnelles avec les partenaires de l\'entreprise', 'En veillant à sa présentation. En connaissant la position de l\'entreprise. En veillant à la fiabilité des informations transmises.', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 2, 'Développer l\'influence de l\'entreprise sur un territoire', 'En développant une stratégie d\'influence territoriale. En développant des partenariats', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 1, 'Accroître les compétences d\'une équipe mono-disciplinaire', 'En établissant un environnement propice à la montée en compétences. En favorisant le partage de pratiques. En transmettant ses savoirs pour rendre autonome les collaborateurs. En donnant des responsabilités propices aux apprentissages.', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 2, 'Accroître les compétences d\'une équipe pluridisciplinaire', 'En facilitant les échanges de pratiques entre les différents emplois. En développant les compétences collectives de l\'équipe.', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 1, 'A remplir', 'A remplir', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 2, 'A remplir', 'A remplir', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 1, 'Suivre un dossier administratif simple', 'En tenant compte des échéances. En respectant les étapes de la procédure. En vérifiant la présence des différentes pièces.', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 2, 'Suivre un dossier administratif complexe', 'En concertant avec es différents acteurs. En identifiant les enjeux et risques.', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sousdomaine`
--

CREATE TABLE `sousdomaine` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `id_domaine` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sousdomaine`
--

INSERT INTO `sousdomaine` (`id`, `nom`, `description`, `id_domaine`, `createdAt`, `updatedAt`) VALUES
(1, 'Communiquer', 'Capacité à se mettre en relation avec les autres et à transmettre quelque chose\r\n\r\n', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Diriger', 'Capacité de conduire l\'activité de quelqu\'un et d\'exercer une influence sur son travail.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Créer', 'Capacité à réaliser quelque chose qui n\'existait pas avant.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Contrôler', 'Capacité à soumettre quelque chose ou quelqu\'un à une vérification.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Conseiller', 'Capacité à indiquer ce qu\'il convient de faire ou pas.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Organiser', 'Capacité à soumettre à une méthode ou à un mode de fonctionnement déterminé.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Développer', 'Capacité à donner de l\'ampleur à un produit, un dispositif.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Produire', 'Capacité à assurer une réalisation matérielle ou non.', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `competence`
--
ALTER TABLE `competence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sous_domaine` (`id_sousdomaine`);

--
-- Indexes for table `direction`
--
ALTER TABLE `direction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domaine`
--
ALTER TABLE `domaine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emploi`
--
ALTER TABLE `emploi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_direction` (`id_direction`),
  ADD KEY `id_niveau` (`id_niveau`);

--
-- Indexes for table `emploi_niveaux`
--
ALTER TABLE `emploi_niveaux`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_emploi` (`id_emploi`),
  ADD KEY `id_niveau` (`id_niveau`);

--
-- Indexes for table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_competencepk` (`id_competence`) USING BTREE;

--
-- Indexes for table `sousdomaine`
--
ALTER TABLE `sousdomaine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_domaine` (`id_domaine`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `competence`
--
ALTER TABLE `competence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `direction`
--
ALTER TABLE `direction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `domaine`
--
ALTER TABLE `domaine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `emploi`
--
ALTER TABLE `emploi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `emploi_niveaux`
--
ALTER TABLE `emploi_niveaux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `niveau`
--
ALTER TABLE `niveau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `sousdomaine`
--
ALTER TABLE `sousdomaine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `competence`
--
ALTER TABLE `competence`
  ADD CONSTRAINT `competence_ibfk_1` FOREIGN KEY (`id_sousdomaine`) REFERENCES `sousdomaine` (`id`);

--
-- Constraints for table `emploi`
--
ALTER TABLE `emploi`
  ADD CONSTRAINT `emploi_ibfk_1` FOREIGN KEY (`id_direction`) REFERENCES `direction` (`id`);

--
-- Constraints for table `emploi_niveaux`
--
ALTER TABLE `emploi_niveaux`
  ADD CONSTRAINT `emploi_niveaux_ibfk_1` FOREIGN KEY (`id_emploi`) REFERENCES `emploi` (`id`),
  ADD CONSTRAINT `emploi_niveaux_ibfk_2` FOREIGN KEY (`id_niveau`) REFERENCES `niveau` (`id`);

--
-- Constraints for table `niveau`
--
ALTER TABLE `niveau`
  ADD CONSTRAINT `niveau_ibfk_1` FOREIGN KEY (`id_competence`) REFERENCES `competence` (`id`);

--
-- Constraints for table `sousdomaine`
--
ALTER TABLE `sousdomaine`
  ADD CONSTRAINT `sousdomaine_ibfk_1` FOREIGN KEY (`id_domaine`) REFERENCES `domaine` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
